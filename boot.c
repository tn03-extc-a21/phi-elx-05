// it is to be written in reset handler

//1)basic initialization
// bss data segment initialization
PFCfgUart0 uart0Config = 
								{
									enPclkDiv_4, 			
									enUart0Baudrate_9600, 	
									enUart0Databits_8, 		
									enUart0ParityNone, 		
									enUart0StopBits_1, 
									enUart0IntTxRx
								};


void ResetHandler(void)
{
   //Reset Sourse identification register is gven a value (RSID)
    RSID =  0010  //this values means processor got  a reset due to sysreset
   {
        	// UART0 Initialization
            	status = pfUart0Open(&uart0Config);
#if( PRIME_DEBUG == 1 ) 
	pfUart0Write("\r\rUART initialization status : ",31);
	pfUart0Write( pfGetErrorString(status),pfStrLen(pfGetErrorString(status)));
#endif
	if(status != enStatusSuccess)
		while(1);
            //establish connection to the server 
            // download the firmware and store it at a location
        //after the firmware is stored in some location 
        //we will copy the adress of newly stored firmware to the PC    

   }
    else 
    {
        main();
    }

}