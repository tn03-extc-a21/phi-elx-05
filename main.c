#include "prime_framework.h"
#include "prime_gpio.h"
#include "prime_hydraBoardConfig.h"
#include "prime_sysClk.h"
#include "prime_uart0.h"
#include "prime_rit.h"
#include "prime_delay.h"
#include "prime_utils.h"

PFCfgClk clkConfig = 			{	
									100000000,
									12000000,
									enPllClkSrcMainOSC
								};

PFCfgUart0 uart0Config = 
								{
									enPclkDiv_4, 			
									enUart0Baudrate_9600, 	
									enUart0Databits_8, 		
									enUart0ParityNone, 		
									enUart0StopBits_1, 
									enUart0IntTxRx
								};
								
PFCfgRit ritConfig 			=
								{
									250,
									0x00,
									pfDelayTickUpdate,
									enPclkDiv_4,
									enBooleanTrue,
									enBooleanTrue
								};
void hydraBoardInit(void)
{
	// PFword count=0;
	
	// system clock initialization
	pfSysSetCpuClock(&clkConfig);
	
	
	// GPIO initialization - UART0 
	PFEnStatus status = pfGpioInit(pfHydraUART0GpioCfg, 2);
	

	// UART0 Initialization
	status = pfUart0Open(&uart0Config);
#if( PRIME_DEBUG == 1 ) 
	pfUart0Write("\r\rUART initialization status : ",31);
	pfUart0Write( pfGetErrorString(status),pfStrLen(pfGetErrorString(status)));
#endif
	if(status != enStatusSuccess)
		while(1);
		
	
	// GPIO initialization - LEDs
	status = pfGpioInit(pfHydraLEDGpioCfg, 3);
#if( PRIME_DEBUG == 1 )
	pfUart0Write("\r\rLED initialization status : ",30);
	pfUart0Write( pfGetErrorString(status),pfStrLen(pfGetErrorString(status)));
#endif
	if(status != enStatusSuccess)
		while(1);
		
	
	// RIT initialization
	status = pfRitOpen(&ritConfig);
#if( PRIME_DEBUG == 1 )
	pfUart0Write("\r\rRIT initialization status : ",30);
	pfUart0Write( pfGetErrorString(status),pfStrLen(pfGetErrorString(status)));
#endif
	if(status != enStatusSuccess)
		while(1);

	pfDelaySetTimerPeriod(30);
	pfRitStart();
}

							
int main(void)
{
	PFEnBoolean ledOn = enBooleanFalse ;
	hydraBoardInit();
	while(1)
	{
		if(ledOn == enBooleanTrue)
		{
			pfGpioPinsClear(HYDRA_LED_UL1_PORT,HYDRA_LED_UL1_PIN);
			pfGpioPinsClear(HYDRA_LED_UL2_PORT,HYDRA_LED_UL2_PIN);
			pfGpioPinsClear(HYDRA_LED_UL3_PORT,HYDRA_LED_UL3_PIN);
			pfUart0Write((PFbyte*)"\rLEDs : ON",10);
		}
		else 
		{
			pfGpioPinsSet(HYDRA_LED_UL1_PORT,HYDRA_LED_UL1_PIN);
			pfGpioPinsSet(HYDRA_LED_UL2_PORT,HYDRA_LED_UL2_PIN);
			pfGpioPinsSet(HYDRA_LED_UL3_PORT,HYDRA_LED_UL3_PIN);
			pfUart0Write((PFbyte*)"\rLEDs : OFF",11);
		}
		ledOn = 1^ledOn ;
		pfDelayMilliSec(5000);
	}				
	return 0;
}